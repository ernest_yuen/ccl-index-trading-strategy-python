# CCL-Index-Trading-Strategy-Python

## 中原城市指數 (CCI)香港地產價格指數

為什麼需要地產價格指數？

投 資 者 和 樓 宇 買 家 需 要 一 些 指 標 去 掌 握 香 港 地 產 市 場 價 格 的 變 動 。 “ 中 原 城 市 指 數 ” 的 創 制 就 是 要 提 供 這 樣 的 資 訊 ， 以 作 為 一 種 反 映 香 港 地 產 市 場 變 動 趨 勢 的 參 考 指 標 。

“ 中 原 城 市 指 數 ” 是 一 個 每 月 發 佈 的 指 數 。 它 是 基 於 政 府 土 地 註 冊 處 登 記 的 住 宅 樓 宇 交 易 紀 錄 編 制 ， 用 以 描 述 地 產 市 場 的 價 格 變 動 。

“ 中 原 城 市 領 先 指 數 ” 是 一 個 每 周 發 佈 的 指 數 。 它 是 基 於 在 中 原 地 產 代 理 有 限 公 司 的 合 約 成 交 價 編 制 ， 用 於 反 映 最 新 的 地 產 市 場 價 格 變 動 。

由 於 登 記 過 程 需 時 ， 利 用 土 地 註 冊 處 的 樓 宇 買 賣 價 格 數 據 編 制 而 成 的 指 數 未 能 提 供 最 新 的 市 場 資 訊 。 然 而 ， 利 用 臨 時 合 約 成 交 價 數 據 編 制 的 指 數 則 可 以 反 映 地 產 市 場 最 新 的 價 格 變 動 。 中 原 地 產 代 理 有 限 公 司 在 地 產 代 理 市 場 的 佔 有 率 超 過 20% ， 所 以 它 的 成 交 數 據 是 能 夠 反 映 市 場 的 主 要 狀 況 的 。


## 屋苑的調整呎價
定 義 ： 呎 價 =每 平 方 英 呎 成 交 價 。

由 於 呎 價 會 受 景 觀 、 方 向 和 層 數 等 因 素 影 響 ， 所 以 利 用 回 歸 分 析 方 法 來 確 定 這 些 因 素 對 呎 價 的 影 響 ， 從 而 計 算 屋 苑 的 調 整 呎 價 。

由 於 一 個 屋 苑 的 不 同 單 位 在 質 素 上 (亦 反 映 在 買 賣 價 格 上 )是 會 有 很 大 差 別 ， 使 用 調 整 呎 價 避 免 了 直 接 使 用 平 均 呎 價 可 能 引 發 的 偏 差 。

最 近 期 的 調 整 呎 價 可 用 於 屋 苑 單 位 價 格 的 評 估 。



## Reference
* http://www1.centadata.com/cci/notes_c.htm
* http://www1.centadata.com/cci/notes_e.htm
* http://www1.centadata.com/cci/CCL-p2.html